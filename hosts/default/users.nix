{
  pkgs,
  username,
  ...
}:

let
  inherit (import ./variables.nix) gitUsername;
in
{
  users.users = {
    "${username}" = {
      homeMode = "755";
      isNormalUser = true;
      description = "${gitUsername}";
      extraGroups = [
        "networkmanager"
        "wheel"
        "libvirtd"
        "scanner"
        "lp"
      ];
      shell = pkgs.bash;
      ignoreShellProgramCheck = true;
      packages = with pkgs; [

    # My packages

    # Misc Utils
    dex
    foot
    figlet
    jellyfin-media-player



    # Internet
    google-chrome
    dig
    ncftp
    gping
    ntp
    putty
    wezterm
    vesktop

    # CLI utils
    mc
    ncdu
    fzf
    sshfs
    zoxide
    most
    dua
    cava
    tmux
    bat
    eza
    zellij
    p7zip
    okular 

    # Monitoring
    atop
    htop
    gtop
    gotop
    glances
    bottom
    stacer
    mission-center
    iotop
    nvtopPackages.full
    ipfetch
    fastfetch
    hyfetch
    cpufetch

    # Programing
    ripgrep
    ugrep
    lazygit
    meld

 #hyprland related
    wpaperd
    nwg-drawer
    nwg-launchers
    nomacs
    hyprlang
    hyprutils
    hyprwayland-scanner
    hyprshot
    pyprland

    # File MGr
    tree
    yazi
    ouch


      ];
    };
    # "newuser" = {
    #   homeMode = "755";
    #   isNormalUser = true;
    #   description = "New user account";
    #   extraGroups = [ "networkmanager" "wheel" "libvirtd" ];
    #   shell = pkgs.bash;
    #   ignoreShellProgramCheck = true;
    #   packages = with pkgs; [];
    # };
  };
}
