{
  pkgs,
  username,
  inputs,
  ...
}:

let
  inherit (import ./variables.nix) gitUsername;
in
{
  users.users = {
    "${username}" = {
      homeMode = "755";
      isNormalUser = true;
      description = "${gitUsername}";
      extraGroups = [
        "networkmanager"
        "wheel"
        "libvirtd"
        "scanner"
        "lp"
      ];
      shell = pkgs.bash;
      ignoreShellProgramCheck = true;
      packages = with pkgs; [


 #  override for aquamarine 
        #           (aquamarine.overrideAttrs (oldAttrs: {
        #  inherit (oldAttrs) pname;
        # version = "0.4.5";
        # }))

   #   override for hyprland 
        #       (hyprland.overrideAttrs (oldAttrs: {
        #inherit (oldAttrs) pname;
        # version = "0.45.0";
        #  }))


    # My packages


 (inputs.ghostty.packages.${pkgs.system}.default)


    # Misc Utils
    dex
        #foot
    figlet
        #jellyfin-media-player
    alacritty
        #tilix
        # st
        #cavalier


    # Internet
        #google-chrome
    dig
    ncftp
    gping
    ntp
    putty
        #wezterm
        # vesktop
    remmina

    # CLI utils
    mc
    ncdu
    fzf
        #sshfs
    zoxide
    most
    dua
   cava
    tmux
    bat
    eza
        # zellij
        #  p7zip
        #  okular 

    # Monitoring
    atop
    htop
    gtop
    gotop
    glances
    bottom
    stacer
        #mission-center
    iotop
    ipfetch
    fastfetch
    hyfetch
    cpufetch
    pfetch

    # Programing
    ripgrep
    ugrep
    lazygit
    meld
    gcc
    gnumake
    cmake
    cargo

 #hyprland related
    wpaperd
    nwg-drawer
    nwg-launchers
    nomacs
    pyprland

    # File MGr
    tree
    yazi
    ouch
    ];
    };
    # "newuser" = {
    #   homeMode = "755";
    #   isNormalUser = true;
    #   description = "New user account";
    #   extraGroups = [ "networkmanager" "wheel" "libvirtd" ];
    #   shell = pkgs.bash;
    #   ignoreShellProgramCheck = true;
    #   packages = with pkgs; [];
    # };
  };
}
