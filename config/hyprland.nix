{
  pkgs,
  lib,
  inputs,
  username,
  host,
  ...
}:

let
  #hyprplugins = inputs.hyprland-plugins.packages.${pkgs.system};
  #hyprplugins = inputs.hyprland-plugins.packages.${pkgs.stdenv.hostPlatform.system};
  inherit (import ../hosts/${host}/variables.nix)
    browser
   borderAnim
    terminal
    extraMonitorSettings
    ;
in
with lib;
{
  wayland.windowManager.hyprland = {
    enable = true;
    xwayland.enable = true;
    systemd.enable = true;
    plugins = [
      pkgs.hyprlandPlugins.hyprtrails 
      pkgs.hyprlandPlugins.hyprexpo 
    ];
    extraConfig =
      let
        modifier = "SUPER";
      in
      concatStrings [
        ''
          env = NIXOS_OZONE_WL, 1
          env = NIXPKGS_ALLOW_UNFREE, 1
          env = XDG_CURRENT_DESKTOP, Hyprland
          env = XDG_SESSION_TYPE, wayland
          env = XDG_SESSION_DESKTOP, Hyprland
          env = GDK_BACKEND, wayland, x11
          env = CLUTTER_BACKEND, wayland
          env = QT_QPA_PLATFORM=wayland;xcb
          env = QT_WAYLAND_DISABLE_WINDOWDECORATION, 1
          env = QT_AUTO_SCREEN_SCALE_FACTOR, 1
          env = SDL_VIDEODRIVER, wayland
          env = MOZ_ENABLE_WAYLAND, 1
          env = HYPRSHOT_DIR /home/dwilliams/Pictures/Screenshots
          exec-once = dbus-update-activation-environment --systemd --all
          exec-once = systemctl --user import-environment QT_QPA_PLATFORMTHEME WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
          exec-once = killall -q swww;sleep .5 && swww init
          exec-once = killall -q waybar;sleep .5 && waybar
          exec-once = killall -q swaync;sleep .5 && swaync
          exec-once = nm-applet --indicator
          exec-once = lxqt-policykit-agent
          exec-once = pypr
          exec-once = swww img /home/${username}/Pictures/Wallpapers/zaney-wallpaper.jpg
          # Disable slidepad when typing
          exec-once = hyprctl keyword input:touchpad:disable_while_typing true
          monitor=Virtual-1,1920x1080@60,auto,1
          monitor=,preferred,auto,1.5
          ${extraMonitorSettings}
          general {
            gaps_in = 6
            gaps_out = 8
            border_size = 4
            layout = dwindle
            resize_on_border = true
          }
          input {
            kb_layout = us
            kb_options = grp:alt_shift_toggle
            kb_options=caps:super
            follow_mouse = 1
            touchpad {
              natural_scroll = false
            }
            sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
            accel_profile = flat
          }

          cursor {
            no_hardware_cursors = true
          }

          #  Window rules
          windowrule = noborder,^(wofi)$
          windowrule = center,^(wofi)$
          windowrule = center,^(steam)$
          windowrulev2 = stayfocused, title:^()$,class:^(steam)$
          windowrulev2 = minsize 1 1, title:^()$,class:^(steam)$


           # windowrule v2 move to workspace
              windowrulev2 = workspace 2, class:^([Gg]oogle-chrome(-stable|-beta|-dev|-unstable)?)$
              windowrulev2 = workspace 3, class:^([Dd]iscord)$
              windowrulev2 = workspace 3, class:^([Ww]ebCord)$
              windowrulev2 = workspace 3, class:^([Vv]esktop)$
              windowrulev2 = workspace 3, class:^([Ff]erdium)$

           # Windowrules v2 float  
              windowrule = float, ^(steam)$
              windowrulev2 = float, class:(xdg-desktop-portal-gtk)
              windowrulev2 = float, class:^(nwg-look|qt5ct|qt6ct)$
              windowrulev2 = float, class:^(nm-applet|nm-connection-editor|blueman-manager)$
              windowrulev2 = float, class:^(file-roller|org.gnome.FileRoller)$ # archive manager
              windowrulev2 = float, class:([Tt]hunar), title:(File Operation Progress)
              windowrulev2 = float, class:([Tt]hunar), title:(Confirm to replace files)

           # Window rule set sizes
              windowrule = size 1080 900, ^(steam)$
              windowrulev2 = size 70% 70%, class:^(gnome-system-monitor|org.gnome.SystemMonitor)$
              windowrulev2 = size 70% 70%, class:^(xdg-desktop-portal-gtk)$
              windowrulev2 = size 60% 70%, title:(Kvantum Manager)
              windowrulev2 = size 60% 70%, class:^(qt6ct)$
              windowrulev2 = size 70% 70%, class:^(evince|wihotspot-gui)$
              windowrulev2 = size 60% 70%, class:^(file-roller|org.gnome.FileRoller)$
              windowrulev2 = size 60% 70%, class:^([Ww]hatsapp-for-linux)$
              windowrulev2 = size 60% 70%, class:^([Ff]erdium)$

            # Window rules opacity
              windowrule = opacity 0.8 0.8, steam
              windowrulev2 = opacity 0.9 0.6, class:^([Rr]ofi)$
              windowrulev2 = opacity 0.9 0.7, class:^(Brave-browser(-beta|-dev)?)$
              windowrulev2 = opacity 0.9 0.8, class:^([Mm]icrosoft-edge(-stable|-beta|-dev|-unstable)?)$
              #windowrulev2 = opacity 0.9 0.8, class:^(google-chrome(-beta|-dev|-unstable)?)$
              #windowrulev2 = opacity 0.94 0.86, class:^(chrome-.+-Default)$ # Chrome PWAs
              windowrulev2 = opacity 0.9 0.8, class:^([Tt]hunar)$
              windowrulev2 = opacity 0.8 0.6, class:^(pcmanfm-qt)$
              windowrulev2 = opacity 0.8 0.7, class:^(gedit|org.gnome.TextEditor)$
              windowrulev2 = opacity 0.9 0.8, class:^(deluge)$
              windowrulev2 = opacity 0.9 0.8, class:^(Alacritty)$
              windowrulev2 = opacity 0.9 0.8, class:^(kitty)$
              windowrulev2 = opacity 0.9 0.7, class:^(mousepad)$
              windowrulev2 = opacity 0.9 0.8, class:^(nwg-look|qt5ct|qt6ct|yad)$
              windowrulev2 = opacity 0.9 0.8, title:(Kvantum Manager)
              windowrulev2 = opacity 0.9 0.7, class:^(com.obsproject.Studio)$
              windowrulev2 = opacity 0.9 0.7, class:^([Aa]udacious)$
              windowrulev2 = opacity 0.9 0.8, class:^(org.gnome.Nautilus)$
              windowrulev2 = opacity 0.9 0.8, class:^(VSCode|code-url-handler)$
              windowrulev2 = opacity 0.9 0.8, class:^(jetbrains-.+)$ # JetBrains IDEs
              windowrulev2 = opacity 0.94 0.86, class:^([Dd]iscord|[Vv]esktop)$
              windowrulev2 = opacity 0.9 0.8, class:^(org.telegram.desktop|io.github.tdesktop_x64.TDesktop)$
              windowrulev2 = opacity 0.94 0.86, class:^(gnome-disks|evince|wihotspot-gui|org.gnome.baobab)$
              windowrulev2 = opacity 0.9 0.8, class:^(file-roller|org.gnome.FileRoller)$ # archive manager
              windowrulev2 = opacity 0.8 0.7, class:^(app.drey.Warp)$ # Warp file transfer
              windowrulev2 = opacity 0.9 0.8, class:^(seahorse)$ # gnome-keyring gui
              windowrulev2 = opacity 0.82 0.75, class:^(gnome-system-monitor|org.gnome.SystemMonitor)$
              windowrulev2 = opacity 0.9 0.8, class:^(xdg-desktop-portal-gtk)$ # gnome-keyring gui
              windowrulev2 = opacity 0.9 0.7, class:^([Ww]hatsapp-for-linux)$
              windowrulev2 = opacity 0.9 0.7, class:^([Ff]erdium)$

            # Center apps
              windowrulev2 = center, class:([Tt]hunar), title:(File Operation Progress)
              windowrulev2 = center, class:([Tt]hunar), title:(Confirm to replace files)
              windowrule = center, ^(steam)$

              
            # windowrule v2 to avoid idle for fullscreen apps
                windowrulev2 = idleinhibit fullscreen, class:^(*)$
                windowrulev2 = idleinhibit fullscreen, title:^(*)$
               windowrulev2 = idleinhibit fullscreen, fullscreen:1


          gestures {
            workspace_swipe = true
            workspace_swipe_fingers = 3
          }
          misc {
            mouse_move_enables_dpms = true
            key_press_enables_dpms = false
          }
          animations {
            enabled = yes
            bezier = wind, 0.05, 0.9, 0.1, 1.05
            bezier = winIn, 0.1, 1.1, 0.1, 1.1
            bezier = winOut, 0.3, -0.3, 0, 1
            bezier = liner, 1, 1, 1, 1
            animation = windows, 1, 6, wind, slide
            animation = windowsIn, 1, 6, winIn, slide
            animation = windowsOut, 1, 5, winOut, slide
            animation = windowsMove, 1, 5, wind, slide
            animation = border, 1, 1, liner
            ${
              if borderAnim == true then
                ''
                  animation = borderangle, 1, 30, liner, loop
                ''
              else
                ''''
            }
            animation = fade, 1, 10, default
            animation = workspaces, 1, 5, wind
          }
          decoration {
            rounding = 0
            #drop_shadow = false


          shadow {
            enabled = true
            range = 6
            render_power = 1

            #color =  $color12
            #color_inactive = $color2
          }

          blur {
            enabled = true	
            popups = false
            special = false
            xray = false
            size = 6
            passes = 2
            ignore_opacity = true
            new_optimizations = true
            }
          }

          #  blur {
          #      enabled = true
          #      size = 5
          #      passes = 3
          #      new_optimizations = on
          #      ignore_opacity = on
          #  }
        #  }
          plugin {
            hyprtrails {
            }
          }
          plugin {
              hyprexpo {
                columns = 3
                gap_size = 5
                 bg_col = rgb(111111)
            workspace_method = center current 
                 # [center/first] [workspace] e.g. first 1 or center m +1
                 enable_gesture = true # laptop touchpad, 4 fingers
                 gesture_distance = 300 # how far is the "max"
                 gesture_positive = true # positive = swipe down. Negative = swipe up.
              }
            }
            bind = CTRL, space, hyprexpo:expo, toggle # can be: toggle, off/disable or on/enable


          dwindle {
            pseudotile = true
            preserve_split = true
          }
          master {
            new_status=master
          }
          
          # Layout Change
          bind = ${modifier}, L, exec, hyprctl keyword general:layout "dwindle"
          bind = ${modifier}SHIFT, L, exec, hyprctl keyword general:layout "master"

          bind = ${modifier},Return,exec,${terminal}
          bind = ${modifier},A,exec, wezterm
          bind = ${modifier}SHIFT,M,exec,nwg-drawer
          bind = ${modifier}SHIFT,Return,exec,rofi-launcher
          bind = ${modifier}SHIFT,D,exec,rofi-launcher
          bind = ${modifier}CTRL,D,exec, nwg-drawer
          bind = ${modifier}SHIFT,W,exec,web-search
          bind = ${modifier}SHIFT,C,exec,wallsetter
          bind = ${modifier}SHIFT,N,exec,swaync-client -rs
          bind = ${modifier},W,exec,${browser}
          bind = ${modifier},E,exec,emopicker9000
          bind = ${modifier},S,exec,screenshootin
          bind = ${modifier} SHIFT,S,exec, hyprshot -m region 
          bind = ${modifier} CTRL,S,exec, hyprshot -m region  --clipboard-only
          bind = ${modifier},D,exec,discord
          bind = ${modifier},O,exec,obs
          bind = ${modifier},C,exec,hyprpicker -a
          bind = ${modifier},G,exec,google-chrome-stable
          bind = ${modifier} SHIFT,G,exec,gimp
          bind = ${modifier},T,exec,thunar
          bind = ${modifier},M,exec,nwg-drawer
          bind = ${modifier},Q,killactive,
          bind = ${modifier},P,pseudo,
          bind = ${modifier}SHIFT,I,togglesplit,
          bind = ${modifier},F,fullscreen,
          bind = ${modifier}SHIFT,F,togglefloating,
          bind = ${modifier}SHIFT,X,exit,
          bind = ${modifier}SHIFT,left,movewindow,l
          bind = ${modifier}SHIFT,right,movewindow,r
          bind = ${modifier}SHIFT,up,movewindow,u
          bind = ${modifier}SHIFT,down,movewindow,d
          bind = ${modifier}SHIFT,h,movewindow,l
          bind = ${modifier}SHIFT,l,movewindow,r
          bind = ${modifier}SHIFT,k,movewindow,u
          bind = ${modifier}SHIFT,j,movewindow,d
          bind = ${modifier},left,movefocus,l
          bind = ${modifier},right,movefocus,r
          bind = ${modifier},up,movefocus,u
          bind = ${modifier},down,movefocus,d
          bind = ${modifier},h,movefocus,l
          bind = ${modifier},l,movefocus,r
          bind = ${modifier},k,movefocus,u
          bind = ${modifier},j,movefocus,d
          bind = ${modifier},1,workspace,1
          bind = ${modifier},2,workspace,2
          bind = ${modifier},3,workspace,3
          bind = ${modifier},4,workspace,4
          bind = ${modifier},5,workspace,5
          bind = ${modifier},6,workspace,6
          bind = ${modifier},7,workspace,7
          bind = ${modifier},8,workspace,8
          bind = ${modifier},9,workspace,9
          bind = ${modifier},0,workspace,10
          bind = ${modifier}SHIFT,SPACE,movetoworkspace,special
          bind = ${modifier},SPACE,togglespecialworkspace
          bind = ${modifier}SHIFT,1,movetoworkspace,1
          bind = ${modifier}SHIFT,2,movetoworkspace,2
          bind = ${modifier}SHIFT,3,movetoworkspace,3
          bind = ${modifier}SHIFT,4,movetoworkspace,4
          bind = ${modifier}SHIFT,5,movetoworkspace,5
          bind = ${modifier}SHIFT,6,movetoworkspace,6
          bind = ${modifier}SHIFT,7,movetoworkspace,7
          bind = ${modifier}SHIFT,8,movetoworkspace,8
          bind = ${modifier}SHIFT,9,movetoworkspace,9
          bind = ${modifier}SHIFT,0,movetoworkspace,10
          bind = ${modifier}CONTROL,right,workspace,e+1
          bind = ${modifier}CONTROL,left,workspace,e-1
          bind = ${modifier},mouse_down,workspace, e+1
          bind = ${modifier},mouse_up,workspace, e-1
          bindm = ${modifier},mouse:272,movewindow
          bindm = ${modifier},mouse:273,resizeactive
          bind = ALT,Tab,cyclenext
          bind = ALT,Tab,bringactivetotop

            # Resize windows
              binde = ${modifier} SHIFT, right, resizeactive, 30 0
              binde = ${modifier} SHIFT, left, resizeactive, -30 0
              binde = ${modifier} SHIFT, up, resizeactive, 0 -30
              binde = ${modifier} SHIFT, down, resizeactive, 0 30

              # Pyprland
              bind = ${modifier} SHIFT, V, exec, pypr toggle volume
              bind = ${modifier} SHIFT, T, exec, pypr toggle term 
              bind = ${modifier} SHIFT, R, exec, pypr reload

          bind = ,XF86AudioRaiseVolume,exec,wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+
          bind = ,XF86AudioLowerVolume,exec,wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-
          binde = ,XF86AudioMute, exec, wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle
          bind = ,XF86AudioPlay, exec, playerctl play-pause
          bind = ,XF86AudioPause, exec, playerctl play-pause
          bind = ,XF86AudioNext, exec, playerctl next
          bind = ,XF86AudioPrev, exec, playerctl previous
          bind = ,XF86MonBrightnessDown,exec,brightnessctl set 5%-
          bind = ,XF86MonBrightnessUp,exec,brightnessctl set +5%
        ''
      ];
  };
}
