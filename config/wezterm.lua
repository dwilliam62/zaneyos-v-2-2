
local config = {}
config.enable_wayland = false
config.font_size = 12
config.color_scheme = 'AdventureTime'

return config

